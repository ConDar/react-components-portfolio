import { DefaultTheme } from "styled-components";

export const AppTheme: DefaultTheme = {
    progressTracker: {
      progress: {
        actualColor: '#b445f5',
        projectedColor: '#d391fa',
        height: '30px',
      },
      checkpoints: {
        scaling: 1.4,
        borderColor: '#7307b3',
        borderWidth: '3px',
        checkmarkColor: 'green',
      }
    }
  }