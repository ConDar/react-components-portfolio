import { LoadingPlaceholder } from 'components/LoadingPlaceholder';
import React, { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { RouteProvider } from 'routes';
import { ThemeProvider } from 'styled-components';
import { AppRouting } from './AppRouting';
import { AppTheme } from './theme';

export const App = () => {
  return (
    <BrowserRouter>
    <RouteProvider>
    <ThemeProvider theme={AppTheme}>
    <Suspense fallback={<SuspenseFallback/>}>
      <AppRouting />
    </Suspense>
    </ThemeProvider>
    </RouteProvider>
    </BrowserRouter>
  );
}

const SuspenseFallback = () => (
  <div className='vw-100 vh-100 d-flex align-items-center justify-content-center'>
    <LoadingPlaceholder textPosition='after'>Loading...</LoadingPlaceholder>
  </div>
)