import React, { useContext, useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { RouteConfig, RouteContext } from 'routes';
import { NotFoundRouteConfig } from 'routes/NotFound';

export const AppRouting = () => {
    const {routes, errorRoutes, setActiveRoute} = useContext(RouteContext);
  
    return (
      <Switch>
        {routes.map(route => (
            <Route key={route.route} path={route.route} exact={true}>
              <ActiveRoute setActiveRoute={setActiveRoute} route={route}/>
            </Route>
        ))}
        
        {errorRoutes.map(route => (
            <Route key={route.route} path={route.route} exact={true}>
              <ActiveRoute setActiveRoute={setActiveRoute} route={route}/>
            </Route>
        ))}
        
        <Route>
          <Redirect to={NotFoundRouteConfig.route}/>
        </Route>
      </Switch>
    );
}

type ActiveRouteProps = {
  setActiveRoute: (route: RouteConfig) => void,
  route: RouteConfig,
}

const ActiveRoute = ({setActiveRoute, route}: ActiveRouteProps) => {
  useEffect(() => {
    setActiveRoute(route)
  }, [route, setActiveRoute]);
  
  return <route.page />;
}