import React from 'react';
import { Container } from 'react-bootstrap';
import { Body } from './Body';
import { Footer } from './Footer';
import { Header } from './Header';
import { Navigation } from './Navigation';

export type PageProps = React.PropsWithChildren<{
    title: string,
}>;

export const Page = ({title, children}: PageProps) => (
    <div className='vh-100 d-flex flex-nowrap flex-column'>
        <Container as='footer'>
            <Navigation />
        </Container>
        <Container className='flex-grow-1 overflow-auto'>
            <Header title={title} />
            <Body>
                {children}
            </Body>
        </Container>
        <Container as='footer'>   
            <Footer title={title}/>
        </Container>
    </div>
);