import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { PageProps } from '.';

export const Body = ({children}: Pick<PageProps, 'children'>) => (
    <Row>
        <Col className='overflow-auto'>
            {children}
        </Col>
    </Row>
);