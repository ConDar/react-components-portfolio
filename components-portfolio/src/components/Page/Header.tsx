import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { PageProps } from ".";

export const Header = ({title}: Pick<PageProps, 'title'>) => <>
    <Row>
        <Col className='text-center'>
            <h1 className='display-3'>{title}</h1>
        </Col>
    </Row>
    <hr/>
</>;