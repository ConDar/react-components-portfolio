import React, { useContext } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { RouteContext } from 'routes';

export const Navigation = () => {
    const {routes, activeRoute, homeRoute} = useContext(RouteContext);

    return (
        <Navbar bg='light' expand='lg'>
            <Navbar.Brand href={homeRoute.route}>{homeRoute.name}</Navbar.Brand>
            <Navbar.Toggle aria-controls='navigation-collapse'/>
            <Navbar.Collapse id='navigation-collapse'>
                <Nav>
                    {routes.filter(route => route !== homeRoute).map(route => (
                        <Nav.Link key={route.route} active={activeRoute === route} href={route.route}>{route.name}</Nav.Link>
                    ))}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}