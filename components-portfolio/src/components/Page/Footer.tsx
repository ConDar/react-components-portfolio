import Copyright from '@material-ui/icons/Copyright';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { PageProps } from '.';

export const Footer = ({title}: Pick<PageProps, 'title'>) => <>
    <hr/>
    <Row className='pb-3'>
        <Col sm className='text-muted'>
            {title}
        </Col>
        <Col sm className='text-muted text-sm-right'>
            <Copyright fontSize={'small'}/> Jake Conkerton-Darby 2020
        </Col>
    </Row>
</>;