import React from 'react';
import styled from 'styled-components';
import { CheckpointsOverlay } from './CheckpointsOverlay';
import { Progress } from './Progress';

export type ProgressTrackerProps = {
    actualProgress: number,
    projectedProgress: number,
    thresholds: number[],
    className?: string,
};

export const ProgressTracker = (props: ProgressTrackerProps) => {
    const {thresholds, className, actualProgress, projectedProgress} = props;

    return (
        <ProgressTrackerWrapper className={className}>
            <Progress
                actualProgress={actualProgress}
                projectedProgress={projectedProgress}
                thresholds={thresholds} />
            <CheckpointsOverlay
                actualProgress={actualProgress}
                thresholds={thresholds} />
        </ProgressTrackerWrapper>
    );
}

const ProgressTrackerWrapper = styled.div`
    position: relative;
    height: ${({theme}) => theme.progressTracker.checkpoints.scaling > 1
        ? `calc(${theme.progressTracker.progress.height} * ${theme.progressTracker.checkpoints.scaling})`
        : theme.progressTracker.progress.height};
    width: ${({theme}) =>
        `calc(100% - (${theme.progressTracker.progress.height} * ${theme.progressTracker.checkpoints.scaling} / 2))`
    };
`;