import React from 'react';
import styled from 'styled-components';
import { Checkmark } from './Checkmark';
import { CheckpointSpacer } from './CheckpointSpacer';
import classNames from 'classnames';

export type CheckpointProps = {
    passed: boolean,
}

const UnstyledCheckpoint = ({passed, className}: CheckpointProps & { className?: string }) => (
    <div className={classNames('checkpoint', className)}>
        {passed && <Checkmark />}
    </div>
);

const StyledCheckpoint = styled(UnstyledCheckpoint)`
    width: calc(${({theme}) => theme.progressTracker.progress.height} * ${({theme}) => theme.progressTracker.checkpoints.scaling});
    height: calc(${({theme}) => theme.progressTracker.progress.height} * ${({theme}) => theme.progressTracker.checkpoints.scaling});
    border: solid ${({theme}) => theme.progressTracker.checkpoints.borderWidth} ${({theme}) => theme.progressTracker.checkpoints.borderColor};
    border-radius: 1000px;
    position: absolute;
    right: calc(0px - (${({theme}) => theme.progressTracker.progress.height} * ${({theme}) => theme.progressTracker.checkpoints.scaling} / 2));
    ${({passed}) => passed && 'background: white;'}
  
    display: inline-flex;
    justify-content: center;
    align-items: center;
  
    ${CheckpointSpacer}:last-of-type & {
  	    background: white;
    }
`;

export const Checkpoint = (props: CheckpointProps) => <StyledCheckpoint {...props} />;