import styled from 'styled-components';

export type CheckpointSpacerProps = {
    threshold: number,
    thresholdMax: number,
}

export const CheckpointSpacer = styled.div.attrs<CheckpointSpacerProps, CheckpointSpacerProps>(props => ({
    ...props,
    className: 'checkpoint-spacer',
}))`
    position: relative;
    width: ${({threshold, thresholdMax}) => `${threshold / thresholdMax * 100}%`};
`;