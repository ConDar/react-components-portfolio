export const useCheckpointsOverlay = (thresholds: number[]) => {
    return {
        thresholdMax: Math.max(...thresholds)
    }
}