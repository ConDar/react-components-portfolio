import React from 'react';
import styled from 'styled-components';
import { CheckpointSpacer } from './CheckpointSpacer';
import { Checkpoint } from './Checkpoint';
import { useCheckpointsOverlay } from './hooks';

export type CheckpointsOverlapyProps = {
    actualProgress: number,
    thresholds: number[],
};

export const CheckpointsOverlay = ({thresholds, actualProgress}: CheckpointsOverlapyProps) => {
    const {thresholdMax} = useCheckpointsOverlay(thresholds);

    return (
        <CheckpointsOverlayWrapper>
            {thresholds.map(threshold => (
                <CheckpointSpacer key={threshold} threshold={threshold} thresholdMax={thresholdMax}>
                    <Checkpoint passed={actualProgress >= threshold} />
                </CheckpointSpacer>
            ))}
        </CheckpointsOverlayWrapper>
    );
}

const CheckpointsOverlayWrapper = styled.div`
    position: absolute;
    width: 100%;
    top: ${({theme}) => theme.progressTracker.checkpoints.scaling > 1
        ? 0
        : `calc((1 - ${theme.progressTracker.checkpoints.scaling}) / 2 * ${theme.progressTracker.progress.height})`};
`;