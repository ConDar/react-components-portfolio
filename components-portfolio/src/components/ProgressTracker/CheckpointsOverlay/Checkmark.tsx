import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import styled from "styled-components";

export const Checkmark = styled(CheckCircleOutline).attrs(({theme}) => ({
    fontSize: theme.progressTracker.checkpoints.checkmarkSize,
    className: 'checkmark'
}))`
    color: ${({theme}) => theme.progressTracker.checkpoints.checkmarkColor};
`