import 'styled-components'

declare module 'styled-components' {
    export interface DefaultTheme {
        progressTracker: {
            progress: {
                actualColor: string,
                projectedColor: string,
                height: string,
            },
            checkpoints: {
                scaling: number,
                borderWidth: string,
                borderColor: string,
                checkmarkColor: string,
                checkmarkSize?: 'small' | 'large',
            }
        }
    }
}