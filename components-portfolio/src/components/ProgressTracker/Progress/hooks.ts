import { clamp } from "helpers/Math";

export const useProgress = (actualProgress: number, projectedProgress: number, thresholds: number[]) => {
    const maxThreshold = Math.max(...thresholds);

    return {
        bothBarsVisible: actualProgress > 0 && actualProgress < projectedProgress,
        actualPercentage: clamp(actualProgress, 0, maxThreshold) / maxThreshold * 100,
        projectedPercentage: clamp(projectedProgress - actualProgress, 0, maxThreshold - actualProgress) / maxThreshold * 100,
    }
}