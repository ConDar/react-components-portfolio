import React from 'react';
import styled from "styled-components";

export type ProgressBarProps = {
    progressPercentage: number,
    bothBarsVisible: boolean,
}

type BaseProgressBarProps = ProgressBarProps &  {
    calcOperator: '+' | '-',
}

const BaseProgressBar = styled.div.attrs<BaseProgressBarProps, BaseProgressBarProps>(props => ({
    ...props,
    className: 'progress-bar',
}))`
    width: ${({progressPercentage, bothBarsVisible, calcOperator, theme}) => bothBarsVisible
        ? `calc(${progressPercentage}% ${calcOperator} (${theme.progressTracker.progress.height} / 2))`
        : `${progressPercentage}%`};
`;

const StyledActual = styled(BaseProgressBar).attrs<ProgressBarProps, BaseProgressBarProps>(props => ({
    ...props,
    calcOperator: '-',
    className: 'actual',
}))`
    border-radius: ${({bothBarsVisible}) => bothBarsVisible ? '0' : '1000px'};

    && {
        background: ${({theme}) => theme.progressTracker.progress.actualColor};
    }
`;

export const Actual = (props: ProgressBarProps) => <StyledActual {...props} />;

const StyledProjected = styled(BaseProgressBar).attrs<ProgressBarProps, BaseProgressBarProps>(props => ({
    ...props,
    calcOperator: '+',
    className: 'projected',
}))`
    border-radius: 0 1000px 1000px 0;

    && {
        background: ${({theme}) => theme.progressTracker.progress.projectedColor};
    }

    ${({bothBarsVisible, theme}) => bothBarsVisible &&
        `&::before {
            width: calc(${theme.progressTracker.progress.height} / 2);
            height: ${theme.progressTracker.progress.height};
            border-radius: 0 1000px 1000px 0;
            background: ${theme.progressTracker.progress.actualColor};
            content: '';
        }`}
`;

export const Projected = (props: ProgressBarProps) => <StyledProjected {...props} />;