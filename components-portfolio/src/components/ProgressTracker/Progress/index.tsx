import React from 'react';
import styled from 'styled-components';
import { useProgress } from './hooks';
import { Actual, Projected } from './ProgressBars';

export type ProgressBarsProps = {
    actualProgress: number,
    projectedProgress: number,
    thresholds: number[],
};

export const Progress = ({actualProgress, projectedProgress, thresholds}: ProgressBarsProps) => {
    const {
        actualPercentage,
        projectedPercentage,
        bothBarsVisible
    } = useProgress(actualProgress, projectedProgress, thresholds);

    return (
        <ProgressWrapper>
            <Actual progressPercentage={actualPercentage} bothBarsVisible={bothBarsVisible} />
            <Projected progressPercentage={projectedPercentage} bothBarsVisible={bothBarsVisible} />
        </ProgressWrapper>
    );
}

const ProgressWrapper = styled.div.attrs(() => ({
    className: 'progress',
}))`
    && {
        height: ${({theme}) => theme.progressTracker.progress.height};
        border-radius: 1000px 0 0 1000px;
        position: absolute;
        top: ${({theme}) => theme.progressTracker.checkpoints.scaling > 1
            ? `calc((${theme.progressTracker.checkpoints.scaling} - 1) / 2 * ${theme.progressTracker.progress.height})`
            : 0};
        left: 0;
        right: 0;
    }
`;