import React from 'react'
import { Spinner } from "react-bootstrap"
import styled from 'styled-components'

export type LoadingPlaceholderProps = {
    spinnerType?: 'border' | 'grow',
} & ({
    children: string,
    textPosition?: 'before' | 'after',
} | {
    children?: undefined,
    textPosition?: undefined,
})

export const LoadingPlaceholder = (props: LoadingPlaceholderProps) => {
    const {
        spinnerType = 'border',
        children: text,
        textPosition: position = 'before',
    } = props;

    return (
        <div className='py-3 d-flex align-items-center text-primary'>
            {text && position === 'before' &&
                <h1 className='mr-3'>{text}</h1>
            }
            <LargeSpinner animation={spinnerType}/>
            {text && position === 'after' &&
                <h1 className='ml-3'>{text}</h1>
            }
        </div>
    )
}

const LargeSpinner = styled(Spinner)`
    width: 4rem;
    height: 4rem;
`