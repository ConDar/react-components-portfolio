import { useEffect, useRef } from "react"

// useMemoCompare based on that https://usehooks.com/useMemoCompare/
export const useMemoCompare = <T>(newValue: T, compare: (oldValue: T, newValue: T) => boolean ): T => {
    const oldValueRef = useRef<T>();
    const oldValue = oldValueRef.current;

    let areEqual = false;
    if (oldValue !== undefined) {
        areEqual = compare(oldValue, newValue);
    }

    useEffect(() => {
        if (!areEqual) {
            oldValueRef.current = newValue;
        }
    })

    if (oldValue === undefined) {
        return newValue;
    } else {
        return areEqual ? oldValue : newValue;
    }
}