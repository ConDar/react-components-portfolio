import React, { ComponentType, ReactChild, useCallback, useMemo } from "react";
import { Button } from "react-bootstrap";
import { ModalType, ModalWithToggle, useModalWithToggle } from "./useModalWithToggle";

export type ConfirmationModalType = ComponentType<{
    title: string,
    onConfirm: () => void,
    bodyContent?: ReactChild,
    confirmButtonText?: string,
    cancelButtonText?: string,
    buttonJustification?: 'left' | 'center' | 'right',
}>

export type ConfirmationModalWithToggle = ModalWithToggle<ConfirmationModalType>;

const createConfirmationModal = (Modal: ModalType, hideModal: () => void): ConfirmationModalType => (props) => {
    const {
        title,
        onConfirm,
        bodyContent,
        confirmButtonText = 'Confirm',
        cancelButtonText = 'Cancel',
        buttonJustification = 'center',
    } = props;

    const onConfirmCallback = useCallback(() => {
        onConfirm();
        hideModal();
    }, [onConfirm]);

    return (
        <Modal>
            <Modal.Header>
                {title}
            </Modal.Header>
            {bodyContent && (
                <Modal.Body>
                    {bodyContent}
                </Modal.Body>
            )}
            <Modal.Footer className={`justify-content-${buttonJustification}`}>
                <Button variant='secondary' onClick={hideModal}>{cancelButtonText}</Button>
                <Button onClick={onConfirmCallback}>{confirmButtonText}</Button>
            </Modal.Footer>
        </Modal>
    );
}

export const useConfirmationModal = (): ConfirmationModalWithToggle => {
    const [Modal, ButtonProxy, {hideModal, ...functions}] = useModalWithToggle();

    const ConfirmationModal = useMemo<ConfirmationModalType>(
        () => createConfirmationModal(Modal, hideModal),
        [Modal, hideModal]);

    return [
        ConfirmationModal,
        ButtonProxy,
        {
            hideModal,
            ...functions
        },
    ];
}