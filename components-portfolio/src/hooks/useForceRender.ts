import { useCallback, useState } from "react";

export const useForceRender = (): (() => void) => {
    const [, setState] = useState<{}>({});

    return useCallback(() => setState({}), []);
}