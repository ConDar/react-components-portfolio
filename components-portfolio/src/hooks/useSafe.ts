import { DependencyList, Dispatch, SetStateAction, useCallback, useState } from "react";
import { useIsMounted } from "./useIsMounted";

export const useCallbackSafe = <T extends (...args: any[]) => any>(callback: T, deps: DependencyList) => {
    const isMounted = useIsMounted();
    const consistentCallback = useCallback(callback, deps);

    return useCallback<(...args: Parameters<T>) => ReturnType<T> | void>((...args) => {
        if (isMounted())
            return consistentCallback(...args);
    }, [isMounted, consistentCallback]);
}

export const useStateSafe = <T extends unknown>(initialState: T | (() => T)): [T, Dispatch<SetStateAction<T>>] => {
    const [state, setState] = useState(initialState);
    const safeSetState = useCallbackSafe(setState, []);

    return [state, safeSetState];
}