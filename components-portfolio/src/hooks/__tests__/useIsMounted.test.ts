import { renderHook } from '@testing-library/react-hooks';
import { useIsMounted } from 'hooks/useIsMounted';

describe('hook useIsMounted', () => {
    it('should return true when mounted', () => {
        const { result } = renderHook(() => useIsMounted());

        expect(result.current()).toBe(true);
    })

    it('should return false when unmounted', () => {
        const { result, unmount } = renderHook(() => useIsMounted());

        unmount();

        expect(result.current()).toBe(false);
    })

    it('should return the same function reference on rerenders', () => {
        const { result, rerender } = renderHook(() => useIsMounted());

        const firstReference = result.current;

        rerender();

        const secondReference = result.current;

        expect(firstReference).toBe(secondReference);
    })
})