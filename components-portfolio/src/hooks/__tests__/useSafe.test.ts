import { act, renderHook } from '@testing-library/react-hooks';
import { useCallbackSafe, useStateSafe } from 'hooks/useSafe';

describe('hook useCallbackSafe', () => {
    it.each([
        0,
        1,
        2,
        -1,
        1000,
    ])('should call the callback when mounted (input = %d)', (input) => {
        const callbackResult = 'Hello, World!'
        const callback = jest.fn((n: number) => callbackResult);
        const { result } = renderHook(() => useCallbackSafe(callback, []));

        act(() => {
            result.current(input);
        })

        expect(callback.mock.calls.length).toBe(1);
        expect(callback.mock.calls[0][0]).toBe(input);
    })

    it.each([
        ['Hello'],
        ['World'],
        [4],
        [[1, 2, 3]]],
    )('should return the result of the callback when mounted (result = %p)', (expectedCallbackResult) => {
        const callback = jest.fn(() => expectedCallbackResult);
        const { result } = renderHook(() => useCallbackSafe(callback, []));

        let callbackResult;
        act(() => {
            callbackResult = result.current();
        })

        expect(callbackResult).toBe(expectedCallbackResult);
    })

    it.each([
        [[1, 2, 3]],
        [['Hello, World!']],
    ])('should return the same callback reference when deps don\'t change (deps = %p)', (deps) => {
        const { result, rerender } = renderHook(({callback, dependencies}) => useCallbackSafe(callback, dependencies), {
            initialProps: {
                callback: jest.fn(),
                dependencies: deps,
            }
        });

        const firstCallbackReference = result.current;

        rerender({
            callback: jest.fn(),
            dependencies: deps,
        });

        const secondCallbackReference = result.current;

        expect(firstCallbackReference).toBe(secondCallbackReference);
    })

    it.each([
        [[1, 2, 3], [1, 2, 4]],
        [[1, 2, 3], [4, 2, 3]],
        [['Foo', 'Bar'], ['Foo', 'Baz']],
        [['Foo', 'Bar'], ['Baz', 'Bar']],
    ])('should return a new callback reference when deps change (initial = %p, new = %p)', (initialDeps, newDeps) => {
        const { result, rerender } = renderHook(({callback, dependencies}) => useCallbackSafe(callback, dependencies), {
            initialProps: {
                callback: jest.fn(),
                dependencies: initialDeps,
            },
        });

        const firstCallbackReference = result.current;

        rerender({
            callback: jest.fn(),
            dependencies: newDeps,
        });

        const secondCallbackReference = result.current;

        expect(firstCallbackReference).not.toBe(secondCallbackReference);
    })

    it('should not call the callback when unmounted', () => {
        const callback = jest.fn();
        const { result, unmount } = renderHook(() => useCallbackSafe(callback, []));

        unmount();
        act(() => {
            result.current();
        })

        expect(callback.mock.calls.length).toBe(0);
    })
})

describe('hook useStateSafe', () => {
    const expectedInitial = 1;
    const expectedNew = 2;

    const incrementState = (n: number) => n + 1;
    const initialState = () => expectedInitial;

    const updateStateTestCases = [
        [expectedInitial, expectedNew],
        [initialState, expectedNew],
        [expectedInitial, incrementState],
        [initialState, incrementState],
    ]
    
    it.each(updateStateTestCases)('should update state when mounted (initial = %p, new = %p)', (initialState, newState) => {
        const { result } = renderHook(() => useStateSafe(initialState));

        expect(result.current[0]).toBe(expectedInitial);

        act(() => {
            result.current[1](newState);
        })

        expect(result.current[0]).toBe(expectedNew);
    })

    it.each(updateStateTestCases)('should not update state when unmounted (initial = %p, new = %p)', (initialState, newState) => {
        const { result, unmount } = renderHook(() => useStateSafe(initialState));

        unmount();
        act(() => {
            result.current[1](newState);
        })

        expect(result.current[0]).toBe(expectedInitial);
    })
})