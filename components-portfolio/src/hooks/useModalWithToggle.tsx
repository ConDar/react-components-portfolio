import React, { useCallback, useMemo, useRef } from 'react';
import { Button, ButtonProps, ModalBody, ModalDialog, ModalFooter, ModalTitle } from 'react-bootstrap';
import { BsPrefixRefForwardingComponent } from 'react-bootstrap/esm/helpers';
import ModalHeader from 'react-bootstrap/esm/ModalHeader';
import Modal, { ModalProps } from 'react-bootstrap/Modal';
import { useForceRender } from './useForceRender';

export type ModalType = BsPrefixRefForwardingComponent<'div', ModalProps> & {
    Body: typeof ModalBody,
    Header: typeof ModalHeader,
    Title: typeof ModalTitle,
    Footer: typeof ModalFooter,
    Dialog: typeof ModalDialog,
    TRANSITION_DURATION: number,
    BACKDROP_TRANSITION_DURATION: number,
}

export type ModalWithToggle<TModal extends unknown = ModalType> = [
    TModal,
    React.ComponentType<ButtonProps>,
    {
        showModal: () => void,
        hideModal: () => void,
        toggleModal: () => void,
    }
];

export const useModalWithToggle = (): ModalWithToggle => {
    const showModal = useRef(false);
    const forceRender = useForceRender();

    const showModalCallback = useCallback(() => {
        showModal.current = true;
        forceRender();
    }, [forceRender]);

    const hideModalCallback = useCallback(() => {
        showModal.current = false;
        forceRender();
    }, [forceRender]);

    const toggleModalCallback = useCallback(() => {
        showModal.current ? hideModalCallback() : showModalCallback();
    }, [hideModalCallback, showModalCallback]);

    const ModalProxy = useMemo<ModalType>(() => {
        const modal = (({onHide, children, ...props}) => {
            const onHideHandler = () => {
                onHide && onHide();
                hideModalCallback();
            };

            return (
                <Modal show={showModal.current} onHide={onHideHandler} {...props}>
                    {children}
                </Modal>
            );
        }) as ModalType;
        
        modal.Body = Modal.Body;
        modal.Header = Modal.Header;
        modal.Title = Modal.Title;
        modal.Footer = Modal.Footer;
        modal.Dialog = Modal.Dialog;
        modal.TRANSITION_DURATION = Modal.TRANSITION_DURATION;
        modal.BACKDROP_TRANSITION_DURATION = Modal.BACKDROP_TRANSITION_DURATION;

        return modal;
    }, [hideModalCallback]);

    const ButtonProxy = useMemo<React.ComponentType<ButtonProps>>(() => ({onClick, ...props}) => {
        const onClickHandler = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
            onClick && onClick(event);
            showModalCallback();
        };
        
        return (
            <Button onClick={onClickHandler} {...props}/>
        );
    }, [showModalCallback]);

    return [
        ModalProxy,
        ButtonProxy,
        {
            showModal: showModalCallback,
            hideModal: hideModalCallback,
            toggleModal: toggleModalCallback,
        }
    ];
}