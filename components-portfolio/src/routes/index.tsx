import React, { useMemo, useState } from 'react';
import { HomeRouteConfig } from './Home';
import { NotFoundRouteConfig } from './NotFound';
import { ProgressTrackerDemoRouteConfig } from './ProgressTrackerDemo';
import { UseModalWithToggleDemoRouteConfig } from './UseModalWithToggleDemo';

const routes: RouteConfig[] = [
    HomeRouteConfig,
    ProgressTrackerDemoRouteConfig,
    UseModalWithToggleDemoRouteConfig,
];

const errorRoutes: RouteConfig[] = [
    NotFoundRouteConfig,
];

export type RouteConfig = {
    name: string,
    route: string,
    page: React.ComponentType,
}

export type RouteContextType = {
    activeRoute: RouteConfig,
    setActiveRoute: (route: RouteConfig) => void,
    routes: RouteConfig[],
    homeRoute: RouteConfig,
    errorRoutes: RouteConfig[],
}

export const RouteContext = React.createContext<RouteContextType>({
    activeRoute: HomeRouteConfig,
    setActiveRoute: () => {},
    routes,
    homeRoute: HomeRouteConfig,
    errorRoutes,
});

export const RouteProvider = ({children}: React.PropsWithChildren<{}>) => {
    const [activeRoute, setActiveRoute] = useState(HomeRouteConfig);

    const contextValue = useMemo<RouteContextType>(() => ({
        activeRoute,
        setActiveRoute,
        routes,
        homeRoute: HomeRouteConfig,
        errorRoutes,
    }), [activeRoute, setActiveRoute]);

    return (
        <RouteContext.Provider value={contextValue}>
            {children}
        </RouteContext.Provider>
    );
}