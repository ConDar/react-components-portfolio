import { lazy } from 'react';
import { RouteConfig } from 'routes';

export const HomeRouteConfig: RouteConfig = {
    page: lazy(() => import('./PageComponent')),
    name: 'Home',
    route: '/',
}