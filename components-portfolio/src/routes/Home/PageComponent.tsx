import classNames from 'classnames';
import { Page } from 'components/Page';
import React from 'react';
import { ProgressTrackerDemoRouteConfig } from '../ProgressTrackerDemo';
import { UseModalWithToggleDemoRouteConfig } from '../UseModalWithToggleDemo';

const Home = () => (
    <Page title={'React Components Portfolio'}>
        <ComponentDescription route={ProgressTrackerDemoRouteConfig.route} name='Progress Tracker'>
            The progress tracker is a component designed to fulfill the need to display a progress through a series of
            checkpoints, as well as providing the ability to also display a projected progress.
            This component uses <Dependency href='https://getbootstrap.com/'>bootstrap</Dependency> for the basis of the progress bars
            and <Dependency href='https://styled-components.com/'>styled-components</Dependency> for much of the styling and
            provide theming support.
        </ComponentDescription>
        
        <ComponentDescription route={UseModalWithToggleDemoRouteConfig.route} name='Use Confirmation Modal'>
            The <code>useModalWithToggle</code> hook is a hook to simplify the logic of implementing a modal.
            Typically a modal will have a button to display it and, starting off hidden, and being shown when clicking the button;
            implementing this functionality usually involves some boilerplate code, which this hook implements.
            This component uses <Dependency href='https://react-bootstrap.github.io/'>react-bootstrap</Dependency>, but could be
            re-implemented using other component libraries such as <Dependency href='https://reactstrap.github.io/'>reactstrap</Dependency>.
        </ComponentDescription>
    </Page>
);

type ComponentDescriptionProps = React.ComponentProps<'div'> & {
    route: string,
    name: string,
};

const ComponentDescription = ({route, name, children, ...props}: ComponentDescriptionProps) => (
    <div {...props}>
        <h1>
            <a href={route}>{name}</a>
        </h1>
        <p>
            {children}
        </p>
    </div>
);

const Dependency = ({className, children, ...props}: React.ComponentProps<'a'>) => (
    <a {...props} className={classNames(className, 'text-monospace', 'bg-light')}>{children}</a>
)

export default Home;