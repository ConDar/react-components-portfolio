import { lazy } from 'react';
import { RouteConfig } from 'routes';

export const NotFoundRouteConfig: RouteConfig = {
    page: lazy(() => import('./PageComponent')),
    name: 'Not Found',
    route: '/404',
}