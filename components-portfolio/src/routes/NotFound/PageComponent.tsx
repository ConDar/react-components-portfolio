import { Page } from 'components/Page';
import React from 'react';

const NotFound = () => (
    <Page title='404: File Not Found'>
        <h1>File Not Found</h1>
        <p>
            Sorry the page you're looking for doesn't exist, please use the navigation bar above to return to one of the existing pages.
        </p>
    </Page>
)

export default NotFound;