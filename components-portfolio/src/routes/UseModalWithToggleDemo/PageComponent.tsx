import { Page } from 'components/Page';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Examples from './Examples';

const UseModalWithToggleDemo = () => {
    return (
        <Page title='Modal Toggle Hook Demo'>
            {Examples.map(([Example, heading]) => (
                <Row key={heading} className='mb-4'>
                    <Col>
                        <h1>{heading}</h1>
                        <Example/>
                    </Col>
                </Row>
            ))}
        </Page>
    )
}

export default UseModalWithToggleDemo;