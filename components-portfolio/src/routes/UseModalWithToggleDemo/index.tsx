import { lazy } from 'react';
import { RouteConfig } from 'routes';

export const UseModalWithToggleDemoRouteConfig: RouteConfig = {
    page: lazy(() => import('./PageComponent')),
    name: 'Modal Toggle Hook',
    route: '/modal-toggle-hook',
}