import React from 'react';
import { ConfirmationModalDemo } from './ConfirmationModalDemo';
import { SimpleDemo } from "./SimpleDemo";

export default [
    [SimpleDemo, "Simple Demo"],
    [ConfirmationModalDemo, "Confirmation Modal Demo"],
] as [React.ComponentType, string][]