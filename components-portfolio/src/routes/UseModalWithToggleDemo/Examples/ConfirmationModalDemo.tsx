import { useConfirmationModal } from 'hooks/useConfirmationModal';
import React from 'react';

const onConfirmCallback = () => alert('Confirmed with modal');

export const ConfirmationModalDemo = () => {
    const [ConfirmationModal, ToggleButton] = useConfirmationModal();

    return <>
        <ToggleButton color='primary'>Show Confirmation Modal</ToggleButton>
        <ConfirmationModal
            title='Confirm the action'
            onConfirm={onConfirmCallback}/>
    </>
}