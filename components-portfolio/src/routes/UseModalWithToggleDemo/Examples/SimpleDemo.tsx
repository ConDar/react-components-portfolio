import { useModalWithToggle } from 'hooks/useModalWithToggle';
import React from 'react';

export const SimpleDemo = () => {
    const [Modal, ToggleButton] = useModalWithToggle();
    
    return <>
        <ToggleButton color='primary'>Show Modal</ToggleButton>
        <Modal>
            <Modal.Header closeButton>
                A simple modal example.
            </Modal.Header>
            <Modal.Body>
                Simple modal body content.
            </Modal.Body>
            <Modal.Footer>
                Simple modal footer.
            </Modal.Footer>
        </Modal>
    </>
}