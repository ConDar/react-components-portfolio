import { lazy } from 'react';
import { RouteConfig } from 'routes';

export const ProgressTrackerDemoRouteConfig: RouteConfig = {
    page: lazy(() => import('./PageComponent')),
    name: 'Progress Tracker',
    route: '/progress-tracker',
}