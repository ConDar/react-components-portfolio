import { clamp } from "helpers/Math";

export type DemoStateAction = {
    type: 'set-actual-progress',
    value: number,
} | {
    type: 'set-projected-progress'
    value: number,
} | {
    type: 'add-threshold',
    value: number,
} | {
    type: 'remove-threshold',
    value: number,
};

export type DemoState = {
    actualProgress: number,
    projectedProgress: number,
    thresholds: number[],
    maximumActual: number,
    maximumProjected: number,
};

const maximumActualMultiplier = 1.25;
const maximumProjectedMultiplier = 1.5;

export const demoStateReducer = (state: DemoState, action: DemoStateAction): DemoState => {
    switch (action.type) {
        case 'set-actual-progress':
            const newActual = clamp(action.value, 0, state.maximumActual);
            return {
                ...state,
                actualProgress: newActual,
                projectedProgress: newActual > state.projectedProgress
                    ? newActual
                    : state.projectedProgress,
            };

        case 'set-projected-progress':
            const newProjected = clamp(action.value, state.actualProgress, state.maximumProjected);
            return {
                ...state,
                projectedProgress: newProjected,
            }

        case 'add-threshold': {
            if (action.value <= 0) {
                return state;
            }
            
            const newThresholds = Array.from(new Set([...state.thresholds, action.value])).sort((a, b) => a - b);
            console.log(newThresholds);
            const maxThreshold = Math.max(...newThresholds);
            return {
                ...state,
                thresholds: newThresholds,
                maximumActual: maxThreshold * maximumActualMultiplier,
                maximumProjected: maxThreshold * maximumProjectedMultiplier,
            };
        }

        case 'remove-threshold':
            const newThresholds = state.thresholds.filter(threshold => threshold !== action.value);
            const maxThreshold = Math.max(...newThresholds);
            const newState: Partial<DemoState> = {
                thresholds: newThresholds,
                maximumActual: maxThreshold * maximumActualMultiplier,
                maximumProjected: maxThreshold * maximumProjectedMultiplier,
            }
            if (newState.maximumActual !== undefined && newState.maximumActual < state.actualProgress) {
                newState.actualProgress = newState.maximumActual;
            }
            if (newState.maximumProjected !== undefined && newState.maximumProjected < state.projectedProgress) {
                newState.projectedProgress = newState.maximumProjected;
            }
            return {
                ...state,
                ...newState,
            };
    }
}