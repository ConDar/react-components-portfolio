import { Page } from 'components/Page';
import React, { useReducer } from 'react';
import { Examples } from './Examples';
import { DemoState, demoStateReducer } from './reducer';
import { Settings } from './Settings';

const initialState: DemoState = {
    actualProgress: 50,
    projectedProgress: 75,
    thresholds: [100],
    maximumActual: 125,
    maximumProjected: 150,
}

const ProgressTrackerDemo = () => {
    const [state, dispatch] = useReducer(demoStateReducer, initialState);

    return (
        <Page title='Progress Tracker Demo'>
            <Settings state={state} dispatch={dispatch} />
            <Examples
                actualProgress={state.actualProgress}
                projectedProgress={state.projectedProgress}
                thresholds={state.thresholds} />
        </Page>
    )
}

export default ProgressTrackerDemo;