import { AppTheme } from 'components/App/theme';
import { ProgressTracker } from 'components/ProgressTracker';
import React from 'react';
import { DefaultTheme, ThemeProvider } from 'styled-components';
import { ExamplesProps } from '.';

const FullyRestyledTheme: DefaultTheme = {
    ...AppTheme,
    progressTracker: {
        progress: {
            height: '15px',
            actualColor: '#29a607',
            projectedColor: '#a69307',
        },
        checkpoints: {
            scaling: 2,
            borderColor: '#072fa6',
            borderWidth: '2px',
            checkmarkColor: '#a10882',
            checkmarkSize: 'small',
        }
    }
}

export const RestyledTrackers = (props: ExamplesProps) => <>
    <div className='row mt-3'>
        <div className='col'>
            <h2>Restyled Trackers</h2>
        </div>
    </div>
    <ThemeProvider theme={FullyRestyledTheme}>
        <div className='row'>
            <div className='col'>
                <ProgressTracker {...props} />
            </div>
        </div>
        <div className='row mt-4'>
            <div className='col'>
                <ThemeProvider theme={theme => ({
                    ...theme,
                    progressTracker: {
                        ...theme.progressTracker,
                        progress: {
                            ...theme.progressTracker.progress,
                            height: '40px',
                        },
                        checkpoints: {
                            ...theme.progressTracker.checkpoints,
                            scaling: 1.2,
                            checkmarkSize: 'large',
                        }
                    }
                })}>
                    <ProgressTracker {...props} />
                </ThemeProvider>
            </div>
            <div className='col d-flex align-items-center'>
                <ThemeProvider theme={theme => ({
                    ...theme,
                    progressTracker: {
                        ...theme.progressTracker,
                        progress: {
                            ...theme.progressTracker.progress,
                            actualColor: '#db7909',
                            projectedColor: '#ffe5c2',
                        },
                        checkpoints: {
                            ...theme.progressTracker.checkpoints,
                            borderColor: '#ffa500',
                            checkmarkColor: '#36a400',
                        }
                    }
                })}>
                    <ProgressTracker {...props} />
                </ThemeProvider>
            </div>
        </div>
    </ThemeProvider>
</>;