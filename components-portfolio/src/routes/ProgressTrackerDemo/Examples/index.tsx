import React from 'react';
import { DefaultTrackers } from './DefaultTrackers';
import { RestyledTrackers } from './RestyledTrackers';

export type ExamplesProps = {
    actualProgress: number,
    projectedProgress: number,
    thresholds: number[],
}

export const Examples = (props: ExamplesProps) => {

    return <>
        <div className='row mt-4'>
            <div className='col'>
                <h1>Examples</h1>
            </div>
        </div>

        <DefaultTrackers {...props} />

        <RestyledTrackers {...props} />
    </>
}