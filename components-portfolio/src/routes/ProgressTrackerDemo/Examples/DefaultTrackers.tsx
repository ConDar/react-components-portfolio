import { ProgressTracker } from 'components/ProgressTracker';
import React from 'react';
import { ExamplesProps } from ".";

export const DefaultTrackers = (props: ExamplesProps) => <>
    <div className='row'>
        <div className='col'>
            <ProgressTracker {...props} />
        </div>
    </div>
    <div className='row mt-4'>
        <div className='col'>
            <ProgressTracker {...props} />
        </div>
        <div className='col'>
            <ProgressTracker {...props} />
        </div>
    </div>
</>;