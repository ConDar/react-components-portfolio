import React, { useState } from 'react';
import styled from "styled-components";
import { DemoState, DemoStateAction } from './reducer';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
import classNames from 'classnames';

export type SettingsProps = {
    state: DemoState,
    dispatch: React.Dispatch<DemoStateAction>,
}

export const Settings = ({state, dispatch}: SettingsProps) => {
    const [newThresholdValue, setNewThresholdValue] = useState<number>(0);

    return <>
        <div className='row'>
            <div className='col'>
                <h1>Settings</h1>
            </div>
        </div>
        <div className='row'>
            <div className='col-lg'>
                <label htmlFor='actual-progress'>Actual Progress ({state.actualProgress})</label>
                <input
                    type='range'
                    className='custom-range'
                    min='0'
                    max={state.maximumActual}
                    value={state.actualProgress}
                    id='actual-progress'
                    onChange={e => dispatch({type: 'set-actual-progress', value: Number.parseInt(e.currentTarget.value)})} />
            </div>
            <div className='col-lg'>
                <label htmlFor='projected-progress'>Projected Progress ({state.projectedProgress})</label>
                <input
                    type='range'
                    className='custom-range'
                    min='0'
                    max={state.maximumProjected}
                    value={state.projectedProgress}
                    id='projected-progress'
                    onChange={e => dispatch({type: 'set-projected-progress', value: Number.parseInt(e.currentTarget.value)})} />
            </div>
        </div>
        <div className='row'>
            <div className='col-md-4 col-lg-2'>
                <label htmlFor='new-threshold'>New Threshold Value</label>
                <div className='input-group'>
                    <input
                        type='number'
                        className='form-control'
                        min='0'
                        value={newThresholdValue}
                        id='new-threshold'
                        onChange={e => setNewThresholdValue(Number.parseInt(e.currentTarget.value))} />
                    <div className='input-group-append'>
                        <button
                            type='button'
                            className={classNames('btn', 'btn-primary', 'py-0', {'disabled': newThresholdValue <= 0})}
                            onClick={() => {
                                dispatch({type: 'add-threshold', value: newThresholdValue});
                            }}
                            disabled={newThresholdValue <= 0}
                        >
                            <AddCircleOutline />
                        </button>
                    </div>
                </div>
            </div>
            <div className='col align-content-center'>
                <label className='d-block'>Thresholds</label>
                {state.thresholds.map(threshold => (
                    <h3 key={threshold} className='d-inline-block mr-2'>
                        <div className='badge badge-pill badge-primary'>
                            {threshold}
                            {state.thresholds.length > 1 && 
                                <CloseThreasholdButton
                                    onClick={() => dispatch({type: 'remove-threshold', value: threshold})}
                                >
                                    &times;
                                </CloseThreasholdButton>
                            }
                        </div>
                    </h3>
                ))}
            </div>
        </div>
    </>
}

const CloseThreasholdButton = styled.button.attrs(() => ({
    className: 'close text-white ml-2'
}))`
    line-height: 0.9em;
    
`;